package com.geekhub.lessons;

/**
 * Created by scherbina on 26.11.2014.
 */

interface Driveable{
    void accelerate(int n);
    void brake();
    void turn(String direction);
    void dashboard();
    boolean ignition(boolean on_off);
    int fuel_refill(int n);
}

abstract class Vehicle implements Driveable{
    private int speed;
    private int max_speed;
    private int carring_capacity;
    private boolean ignition_on;
    private int fuel_per_hour;
    private int tank_volume;
    private int tank_indicator;
    private int wheel_direction;
    private int max_rpm;
    private int rpm_indicator;

    Vehicle(int max_speed, int max_rpm, int carring_capacity, int tank_volume, int fuel_per_hour){
        this.max_speed = max_speed;
        this.max_rpm = max_rpm;
        this.carring_capacity = carring_capacity;
        this.tank_volume = tank_volume;
        this.fuel_per_hour = fuel_per_hour;
        ignition_on = false;
        tank_indicator = 0;
        speed = 0;
        wheel_direction = 0;
    }

    public void dashboard(){
        System.out.println("Ignition is " + (ignition_on ? "ON" : "OFF"));
        System.out.println("Speed: " + speed);
        System.out.println("RPM: " + rpm_indicator);
        System.out.println("Fuel: " + tank_indicator);
        System.out.println("Wheel direction: " + wheel_direction);
    };

    public void features(){
        System.out.println("Max speed: " + max_speed);
        System.out.println("Max RPM: " + max_rpm);
        System.out.println("Tank volume: " + tank_volume);
        System.out.println("Carring capacity: " + carring_capacity);
    };

    private void engine(int n) {
        rpm_indicator = max_rpm * n / 100;
        speed = max_speed * n / 100;
    }

    private void wheels() {}

    private void fuel_tank(int n) {
        tank_indicator += n;
        if (tank_indicator < 5) System.out.println("Warning, few fuel.");
        if (tank_indicator <= 0) ignition(false);
    }

    public boolean ignition(boolean on_off) {
        if (on_off && (tank_indicator > 0)) ignition_on = on_off;
        else {
            rpm_indicator = 0;
            speed = 0;
        }
        return ignition_on;
    }

    public void accelerate(int n) {
        if (!ignition_on) {
            System.out.println("Ignition is " + (ignition_on ? "ON" : "OFF"));
            System.out.println("Nothing to do.");
            return;
        }
        if (n>=0 && n<=100) {
            engine(n);
        }
        else
            System.out.println("Input namber from 0 to 100");
    }

    public void brake() {
        accelerate(0);
    }

    public void turn(String direction) {
    }

    public int getTank_volume() {
        return tank_volume;
    }

    public int getTank_indicator() {
        return tank_indicator;
    }

    public int fuel_refill(int n){
        fuel_tank(n);
        return 0;
    }
}

//class Boat extends Vehicle implements Vehicle.Driveable{}

class Solar_Car extends Vehicle{
    Solar_Car(int max_speed, int max_rpm, int carring_capacity, int tank_volume, int fuel_per_hour){
        super(max_speed, max_rpm, carring_capacity, tank_volume, fuel_per_hour);
    }

    public void accelerate(int n) {
        super.accelerate(n);
    }

    public void brake() {
        super.brake();
    }

    public void turn(String direction) {
        super.turn(direction);
    }

    public boolean ignition(boolean on_off) {
        return super.ignition(on_off);
    }


    public int fuel_refill(int n){
        int rest;

        rest=getTank_indicator()+n-getTank_volume();
        if (rest < 0) super.fuel_refill(n);
        else {
            super.fuel_refill(n-rest);
            System.out.println("Tank is full.");
        }
        return rest;
    }
}

public class LessonSecond {
    public static void main(String args[]){
        Solar_Car my_car = new Solar_Car(100, 5000, 300, 100, 10);
        my_car.features();
        my_car.dashboard();
        my_car.ignition(true);
        my_car.accelerate(50);
        my_car.dashboard();
        my_car.fuel_refill(111);
        my_car.ignition(true);
        my_car.accelerate(50);
        my_car.dashboard();
    }
}
