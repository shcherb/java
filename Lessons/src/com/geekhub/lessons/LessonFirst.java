package com.geekhub.lessons;

/**
 * First Lesson
 */
class MagicNum{
    // Проверяем отрицательное или положительное число. True - отрицательное, False - положительное)
    private boolean isnegative(int n){
        if (n<0) {
            System.out.println("Only positive number!");
            return true;
        } else return false;
    }

    //вычисление факториала в цикле
    int factorial(int n){
        int m=1;

        if (isnegative(n)) return 0;

        for (int i=1; i<=n; i++) {
            m*=i;
        }
        return m;
    }

    //вычисление факториала рекурсией
    int fact(int n){
        int result;

        if (isnegative(n)) return 0;

        if (n==1) return 1;
        result = fact(n-1)*n;
        return result;
    }

    //вывод последовательности Фибоначчи
    void fibonacci(int n){
        int f=0;
        int f2=0;
        int f1=1;

        if (isnegative(n)) return;

        System.out.print(" " + f);
        for (int i=1; i<=n; i++){
            f=f1+f2;
            f1=f2;
            f2=f;
            System.out.print(" "+f);
        }
    }

    //преобразование числа в строковую форму. Тут раскладываем число на сотни, десятки, единицы
    void numb_to_str1(int n) {
        String str="";

        if (n==0) {
            System.out.println(n + " -> "+convert1(n, ""));
            return;
        }
        if ((n / 100) != 0) str=str+convert1(n / 100, " hundred ");
        if (((n % 100) >= 10)&&((n % 100) < 20)) str=str+convert1((n % 100), "teen ");
        else {
            if ((n % 100) >= 20) str=str+convert1((n % 100), "ty ");
            if ((n % 10) != 0) str=str+convert1((n % 10), "");
        }
        System.out.println(n + " -> " + str);
    }

    //тут сопоставляем цифру со строковым написанием. используем управляющую конструкцию if
    private String convert1(int n, String suf){
         String str="";

         if (n==0)
             str="zero";
         else if (n==1)
             str="one"+suf;
         else if (n==2)
             str="two"+suf;
         else if (n==3)
             str="three"+suf;
         else if (n==4 || n==14 || (n>=40 && n<=49))
             str="four"+suf;
         else if (n==5)
             str="five"+suf;
         else if (n==6 || n==16 || (n>=60 && n<=69))
             str="six"+suf;
         else if (n==7 || n==17 || (n>=70 && n<=79))
             str="seven"+suf;
         else if (n==8)
             str="eight";
         else if (n==9 || n==19 || (n>=90 && n<=99))
             str="nine"+suf;
         else if (n==10)
             str="ten";
         else if (n==11)
             str="eleven";
         else if (n==12)
             str="twelve";
         else if (n==13 || (n>=30 && n<=39))
             str="thir"+suf;
         else if (n==15 || (n>=50 && n<=59))
             str="fif"+suf;
         else if (n==18 || (n>=80 && n<=89))
             str="eigh"+suf;
         else if (n>=20 && n<=29)
             str="twen"+suf;
         return str;
    }

    //преобразование числа в строковую форму. Тут раскладываем число на сотни, десятки, единицы
    void numb_to_str2(int n) {
        String str="";
        int digit1, digit2, digit3;

        if (n==0) {
            System.out.println(n + " -> "+convert2(n, ""));
            return;
        }
        digit1 = n / 100;
        digit2 = (n % 100);
        digit3 = n % 10;
        if (digit1 != 0) str=str+convert2(digit1, " hundred ");
        if ((digit2 >= 10) && (digit2 < 20)) str=str+convert2(digit2, "teen ");
        else {
            if (digit2 >= 20) str=str+convert2(digit2-digit3,"ty ");
            if (digit3 != 0) str=str+convert2(digit3,"");
        }
        System.out.println(n + " -> " + str);
    }
    //тут сопоставляем цифру со строковым написанием. используем управляющую конструкцию switch
    private String convert2(int n, String suf){
        String str="";

        switch (n) {
            case 0:
                str="zero";
                break;
            case 1 :
                str="one"+suf;
                break;
            case 2 :
                str="two"+suf;
                break;
            case 3 :
                str="three"+suf;
                break;
            case 4 :
            case 14 :
            case 40 :
                str="four"+suf;
                break;
            case 5 :
                str="five"+suf;
                break;
            case 6 :
            case 16 :
            case 60 :
                str="six"+suf;
                break;
            case 7 :
            case 17 :
            case 70 :
                str="seven"+suf;
                break;
            case 8 :
                str="eight";
                break;
            case 9 :
            case 19 :
            case 90 :
                str="nine"+suf;
                break;
            case 10 :
                str="ten";
                break;
            case 11 :
                str="eleven";
                break;
            case 12 :
                str="twelve";
                break;
            case 13 :
            case 30 :
                str="thir"+suf;
                break;
            case 15 :
            case 50 :
                str="fif"+suf;
                break;
            case 18 :
            case 80 :
                str="eigh"+suf;
                break;
            case 20 :
                str="twen"+suf;
        }
        return str;
    }
}

public class LessonFirst {
    public static void main(String args[]){
        MagicNum mymagicnum=new MagicNum();
        int num=7;
        System.out.println("Hello world!");
        System.out.println("Factorial for "+num+" is "+mymagicnum.factorial(num));
        System.out.println("One else method for calculate Factorial for "+num+": "+mymagicnum.fact(num));
        System.out.println("Fibonacci for " + num + " is ");
        mymagicnum.fibonacci(num);
        System.out.println();
        for (int i=0; i<=200; i++) mymagicnum.numb_to_str1(i);
        for (int i=0; i<=200; i++) mymagicnum.numb_to_str2(i);
    }
}
